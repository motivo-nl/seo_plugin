<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_seo', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('seo_model_id')->nullable()->default(null)->index();
            $table->string('seo_model_type')->nullable()->default(null)->index();

            $table->json('seo_title')->nullable()->default(null);
            $table->json('seo_description')->nullable()->default(null);

            $table->boolean('seo_indexable')->default(false);
            $table->string('seo_canonical_url')->nullable()->default(null);
            $table->string('seo_og_image')->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_seo');
    }
}
