<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSeoTableRemovePrefixes extends Migration
{
    public function up(): void
    {
        Schema::table('liberiser_seo', function (Blueprint $table) {
            $table->renameColumn('seo_model_id', 'model_id')->nullable()->default(null)->index();
            $table->renameColumn('seo_model_type', 'model_type')->nullable()->default(null)->index();

            $table->renameColumn('seo_title', 'title')->nullable()->default(null);
            $table->renameColumn('seo_description', 'description')->nullable()->default(null);

            $table->renameColumn('seo_indexable', 'indexable')->default(false);
            $table->renameColumn('seo_canonical_url', 'canonical_url')->nullable()->default(null);
            $table->renameColumn('seo_og_image', 'og_image')->nullable()->default(null);
        });
    }

    public function down(): void
    {
        Schema::table('liberiser_seo', function (Blueprint $table) {
            $table->renameColumn('model_id', 'seo_model_id')->nullable()->default(null)->index();
            $table->renameColumn('model_type', 'seo_model_type')->nullable()->default(null)->index();

            $table->renameColumn('title', 'seo_title')->nullable()->default(null);
            $table->renameColumn('description', 'seo_description')->nullable()->default(null);

            $table->renameColumn('indexable', 'seo_indexable')->default(false);
            $table->renameColumn('canonical_url', 'seo_canonical_url')->nullable()->default(null);
            $table->renameColumn('og_image', 'seo_og_image')->nullable()->default(null);
        });
    }
}
