<?php

namespace Motivo\Liberiser\Seo\Mixins;

use Closure;
use Motivo\Liberiser\Seo\Models\Seo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class SeoMixins
{
    public function seo(): Closure
    {
        return function (): MorphOne {
            return $this->morphOne(Seo::class, 'model');
        };
    }

    public function getSeoTitle(string $fallback = 'title'): Closure
    {
        return function () use ($fallback): string {
            return (! empty($this->seo->title)) ? $this->seo->title : $this->{$fallback};
        };
    }
}
