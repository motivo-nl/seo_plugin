<?php

namespace Motivo\Liberiser\Seo\Http\Requests;

use Motivo\Liberiser\Seo\Models\Seo;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\StoreLiberiserRequest;

class StoreSeoRequest extends StoreLiberiserRequest
{
    protected $model = Seo::class;

    public static function getRules(LiberiserRequest $request): array
    {
        return [
            'seo_canonical_url' => [
                'nullable',
                'url',
            ],
            'seo_title' => [
                'nullable',
                'max:255',
            ],
            'seo_description' => [],
            'seo_og_image' => [
                'nullable',
                'image',
            ],
        ];
    }
}
