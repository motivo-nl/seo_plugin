<?php

namespace Motivo\Liberiser\Seo\Models;

use Illuminate\Support\Facades\Schema;
use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Seo extends BaseModel
{
    protected $table = 'liberiser_seo';

    protected $fillable = ['indexable', 'title', 'description', 'og_image', 'canonical_url'];

    public static $translatableFields = ['title', 'description'];

    public function model(): MorphTo
    {
        return $this->morphTo('model');
    }

    public static function getModuleName(): string
    {
        return 'seo';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        return [
            [
                'name' => 'seo',
                'relation' => static::getModuleName(),
                'type' => 'liberiser_relation',
                'relationFields' => [
                    [
                        'name' => 'indexable',
                        'type' => 'checkbox',
                        'label' => ucfirst(trans('LiberiserSeo::seo.fields.indexable')),
                        'model' => self::class,
                        'entity' => 'seo',
                        'morph' => true,
                    ],
                    [
                        'name' =>'canonical_url',
                        'type' => 'text',
                        'label' => ucfirst(trans('LiberiserSeo::seo.fields.canonical')),
                        'model' => self::class,
                        'entity' => 'seo',
                        'morph' => true,
                    ],
                    [
                        'name' =>'title',
                        'type' => 'text',
                        'label' => ucfirst(trans('LiberiserSeo::seo.fields.title')),
                        'model' => self::class,
                        'entity' => 'seo',
                        'morph' => true,
                    ],
                    [
                        'name' =>'description',
                        'type' => 'text',
                        'label' => ucfirst(trans('LiberiserSeo::seo.fields.description')),
                        'model' => self::class,
                        'entity' => 'seo',
                        'morph' => true,
                    ],
                    [
                        'name' =>'og_image',
                        'type' => 'upload',
                        'upload' => true,
                        'disk' => 'public',
                        'label' => ucfirst(trans('LiberiserSeo::seo.fields.og_image')),
                        'model' => self::class,
                        'entity' => 'seo',
                        'morph' => true,
                    ],
                ],
            ],
        ];
    }

    public static function getComponentColumns(): array
    {
        return [
            [
                'name' => 'seo_title',
                'label' => ucfirst(trans('LiberiserSeo::seo.fields.title')),
                'type' => 'closure',
                'function' => function ($entry) {
                    return $entry->seo ? $entry->seo->seo_title : null;
                },
            ],
            [
                'name' => 'seo_description',
                'label' => ucfirst(trans('LiberiserSeo::seo.fields.description')),
                'type' => 'closure',
                'function' => function ($entry) {
                    return $entry->seo ? $entry->seo->seo_description : null;
                },
            ],
            [
                'name' => 'seo_og_image',
                'label' => ucfirst(trans('LiberiserSeo::seo.fields.og_image')),
                'type' => 'closure',
                'function' => function ($entry) {
                    return $entry->seo ? $entry->seo->seo_og_image : null;
                },
            ],
        ];
    }

    public static function getComponentRelations(): array
    {
        if (! Schema::hasTable('liberiser_seo')) {
            return [];
        }

        return ['seo'];
    }

    public function getDisplayNameAttribute(): string
    {
        return $this->seo_title;
    }

    public function setOgImageAttribute($value): void
    {
        $attributeName = 'og_image';
        $disk = 'public';
        $destinationPath = 'og_images';
        $fileName = 'seo_og_image';

        $this->uploadFileToDisk($value, $attributeName, $disk, $destinationPath, $fileName);
    }
}
