<?php

namespace Motivo\Liberiser\Seo;

use Illuminate\Support\ServiceProvider;

class SeoServiceProvider extends ServiceProvider
{
    /** @var string */
    protected $publishablePrefix = 'liberiser';

    public function register(): void
    {
        $this->loadViewsFrom(__DIR__.'/resources/views', 'seo');

        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->mergeConfigFrom(__DIR__.'/../config/seo.php', 'liberiser.seo');
    }

    public function boot(): void
    {
        $this->registerPublishing();

        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'LiberiserSeo');
    }

    private function registerPublishing(): void
    {
        $this->publishes([
            __DIR__.'/../config/seo.php' => config_path('liberiser/seo.php'),
        ], 'liberiser.seo');
    }
}
