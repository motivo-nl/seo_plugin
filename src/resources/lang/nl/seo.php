<?php

return [
    'columns' => [
        'indexable' => 'Indexeerbaar',
        'canonical' => 'Canonical',
        'title' => 'SEO Titel',
        'description' => 'SEO Omschrijving',
        'og_image' => 'OG Afbeelding',
    ],
    'fields' => [
        'indexable' => 'Indexeerbaar',
        'canonical' => 'Canonical',
        'title' => 'SEO Titel',
        'description' => 'SEO Omschrijving',
        'og_image' => 'OG Afbeelding',
    ],
];
