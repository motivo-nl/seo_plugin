<?php

return [
    'columns' => [
        'indexable' => 'Indexable',
        'canonical' => 'Canonical URL',
        'title' => 'SEO Title',
        'description' => 'SEO Description',
        'og_image' => 'OG Image',
    ],
    'fields' => [
        'indexable' => 'Indexable',
        'canonical' => 'Canonical URL',
        'title' => 'SEO Title',
        'description' => 'SEO Description',
        'og_image' => 'OG Image',
    ],
];
