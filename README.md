# Seo plugin for Liberiser

This is a plugin for Motivo\Liberiser modules. It adds translatable seo fields to the module.

## Installation

You can install the package via composer:

```bash
composer require motivo/liberiser-seo
```

## Usage

Add the plugin to a module in the config file liberiser/config.php

Example:
``` php
'modules' => [
    'pages' => [
        'plugins' => [
            'seo' => true,
        ],
        'active' => true,
    ],
],
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
