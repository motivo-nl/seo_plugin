<?php

use Motivo\Liberiser\Seo\Models\Seo;
use Motivo\Liberiser\Seo\Mixins\SeoMixins;
use Motivo\Liberiser\Seo\Http\Requests\StoreSeoRequest;
use Motivo\Liberiser\Seo\Http\Requests\UpdateSeoRequest;

return [
    'class' => Seo::class,
    'mixins' => SeoMixins::class,
    'store_request' => StoreSeoRequest::class,
    'update_request' => UpdateSeoRequest::class,
];
