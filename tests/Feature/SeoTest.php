<?php

namespace Tests\Feature;

use Str;
use App\User;
use Tests\TestCase;
use Illuminate\Http\Response;
use App\Http\Middleware\CheckIfAdmin;
use Motivo\Liberiser\Pages\Models\Page;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Foundation\Testing\WithFaker;
use Motivo\Liberiser\Base\Models\Permission;

class SeoTest extends TestCase
{
    use WithFaker;

    private const LOCALE_NL = 'nl';

    private const LOCALE_EN = 'en';

    public function setUp(): void
    {
        parent::setUp();

        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->withoutMiddleware(CheckIfAdmin::class);

        $this->be(new User([
            'id' => 1,
            'first_name' => 'User',
            'last_name' => 'Name',
            'role' => Permission::ROLE_ADMIN,
        ]));

        $this->artisan('db:seed', ['--class' => 'Motivo\\Liberiser\\Base\\Database\\Seeds\\LanguageSeeder']);
    }

    /** @test */
    public function create_seo_test(): void
    {
        $locale = static::LOCALE_NL;

        $inputData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'seo_indexable' => $this->faker->boolean,
            'seo_title' => $this->faker->name,
            'seo_description' => $this->faker->paragraph,
            'seo_canonical_url' => $this->faker->url,
        ];

        $this->createMenu();
        $this->createPage(0, 1, $inputData);

        $link = DIRECTORY_SEPARATOR.Str::slug($inputData['title']);

        $this->assertDatabaseHas('liberiser_pages', ["title->{$locale}" => $inputData['title']]);
        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$locale}" => $link]);
        $this->assertDatabaseHas('liberiser_seo', ["title->{$locale}" => $inputData['seo_title']]);
    }

    /** @test */
    public function update_seo_test(): void
    {
        $locale = static::LOCALE_NL;

        $inputData = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $locale,
            'seo_indexable' => $this->faker->boolean,
            'seo_title' => $this->faker->name,
            'seo_description' => $this->faker->paragraph,
            'seo_canonical_url' => $this->faker->url,
        ];

        $this->createMenu();
        $page = $this->createPage(0, 1, $inputData);

        $updateData = [
            'id' => $page->id,
            'title' => $inputData['title'],
            'locale' => $locale,
            'seo_indexable' => $this->faker->boolean,
            'seo_title' => $this->faker->name,
            'seo_description' => $this->faker->paragraph,
            'seo_canonical_url' => $this->faker->url,
        ];

        $page = $this->updatePage($page, $updateData);

        $link = DIRECTORY_SEPARATOR.Str::slug($inputData['title']);

        $this->assertDatabaseHas('liberiser_pages', ["title->{$locale}" => $inputData['title']]);
        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$locale}" => $link]);
        $this->assertDatabaseHas('liberiser_seo', ["title->{$locale}" => $updateData['seo_title']]);
        $this->assertDatabaseMissing('liberiser_seo', ["title->{$locale}" => $inputData['seo_title']]);
    }

    /** @test */
    public function multilanguage_seo_test(): void
    {
        $nl = static::LOCALE_NL;
        $en = static::LOCALE_EN;

        $dataNl = [
            'title' => $this->faker->name,
            'content' => $this->faker->paragraph,
            'locale' => $nl,
            'seo_indexable' => $this->faker->boolean,
            'seo_title' => $this->faker->name,
            'seo_description' => $this->faker->paragraph,
            'seo_canonical_url' => $this->faker->url,
        ];

        $this->createMenu();
        $page = $this->createPage(0, 1, $dataNl);

        $dataEn = [
            'id' => $page->id,
            'title' => $this->faker->name,
            'locale' => $en,
            'seo_indexable' => $this->faker->boolean,
            'seo_title' => $this->faker->name,
            'seo_description' => $this->faker->paragraph,
            'seo_canonical_url' => $this->faker->url,
        ];

        $page = $this->updatePage($page, $dataEn);

        $linkNl = DIRECTORY_SEPARATOR.Str::slug($dataNl['title']);
        $linkEn = DIRECTORY_SEPARATOR.Str::slug($dataEn['title']);

        $this->assertDatabaseHas('liberiser_pages', ["title->{$nl}" => $dataNl['title']]);
        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$nl}" => $linkNl]);
        $this->assertDatabaseHas('liberiser_seo', ["title->{$nl}" => $dataNl['seo_title']]);
        $this->assertDatabaseHas('liberiser_pages', ["title->{$en}" => $dataEn['title']]);
        $this->assertDatabaseHas('liberiser_menu_items', ["link->{$en}" => $linkEn]);
        $this->assertDatabaseHas('liberiser_seo', ["title->{$en}" => $dataEn['seo_title']]);
        $this->assertDatabaseMissing('liberiser_seo', ["title->{$nl}" => $dataEn['seo_title']]);
        $this->assertDatabaseMissing('liberiser_seo', ["title->{$en}" => $dataNl['seo_title']]);
    }

    private function createPage(int $parentType, int $parentId, array $data): Page
    {
        $data = array_merge($data, ['parent_type' => $parentType, 'parent_id' => $parentId]);

        $this->post('/admin/page', $data)->assertStatus(Response::HTTP_FOUND);

        return Page::orderby('id', 'DESC')->first();
    }

    private function updatePage(Page $page, array $data): Page
    {
        $data = array_merge($data, ['id' => $page->id]);

        $this->put('/admin/page/'.$page->id, $data)->assertStatus(Response::HTTP_FOUND);

        return $page->refresh();
    }

    private function createMenu(int $amount = 1): void
    {
        for ($i = 0; $i < $amount; $i++) {
            $inputData = [
                'name' => $this->faker->name,
            ];

            $this->post('/admin/page/menu', $inputData)->assertStatus(Response::HTTP_FOUND);
        }
    }
}
